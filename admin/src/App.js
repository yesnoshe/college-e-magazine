import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import CreateUser from "./Pages/CreateUser";

import ErrorPage from "./Pages/ErrorPage";
import Login from "./Pages/Login";
import Main from "./Pages/Main";
import NewPass from "./Pages/NewPass";
import PassReset from "./Pages/PassReset";
import News from "./Pages/News";
import Conversation from "./Pages/Conversation";
import Literature from "./Pages/Literature";
import Photography from "./Pages/Photography";
import Users from "./Pages/Users";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Main />} />
        <Route path="/reset" element={<PassReset />} />
        <Route path="/new" element={<NewPass />} />
        <Route path="/add" element={<CreateUser />} />
        <Route path="/news" element={<News />} />
        <Route path="/conversation" element={<Conversation />} />
        <Route path="/literature" element={<Literature/>} />
        <Route path="/photography" element={<Photography />} />
        <Route path="/users" element={<Users />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </Router>
  );
}

export default App;
