import React from 'react'
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import "../css/PassReset.css";
import FloatingLabel from "react-bootstrap/esm/FloatingLabel";
import { useNavigate } from "react-router-dom";


function PassReset() {
    let navigate = useNavigate();
  return (
    <Container className='cont'>
        <h5 className='mb-3'>Reset Password</h5>
        <Form onSubmit={() => {
          navigate("/new"); 
        }}>
        <FloatingLabel label="Enter your email here" className="mb-1 mt-4" controlId="formBasicEmail">
          <Form.Control className="inp" type="email" placeholder="Enter email" />
        </FloatingLabel>
        <p className='mb-5'>A link to reset your password will be sent to your email. Please wait a moment.</p>
        <Button className="btnlg" variant="mt-5 w-100" type="submit">
          Send Link
        </Button>
      </Form>
    </Container>
  )
}

export default PassReset