import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import "../css/Login.css";
import loginicon from "../images/Logo black.jpeg";
import { Link, useNavigate } from "react-router-dom";
import FloatingLabel from "react-bootstrap/esm/FloatingLabel";

function Login() {
  let navigate = useNavigate();
  return (
    <Container className="login text-center">
      <img className="icon-img logoimge" src={loginicon} alt="icon" />
      <h4 className="mt-3">Admin</h4>
      <Form
        onSubmit={() => {
          navigate("/home"); 
        }}
        className="p-4"
      >
        <FloatingLabel label="Email" className="mb-3 mt-4" controlId="formBasicEmail">
          <Form.Control className="inp" type="email" placeholder="Enter email" />
        </FloatingLabel>

        <FloatingLabel label="Password" className="mb-4" controlId="formBasicPassword">
          <Form.Control className="inp" type="password" placeholder="Password" />
        </FloatingLabel>

        <Button className="btnlg" variant="mt-5 w-100" type="submit">
          Sign In
        </Button>
      </Form>
      <p className="text-start mt-3"><Link className="frgtpass" to="/reset">Forgot Password?</Link></p>

      
    </Container>
  );
}

export default Login;
