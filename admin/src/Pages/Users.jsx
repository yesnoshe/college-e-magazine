import React from "react";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import loginicon from "../images/Logo black.jpeg";

import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

const Users = () =>{
    return(
        <>
        <Navbar collapseOnSelect expand="lg" className="navadmin mb-5 py-3">
        <Container>
          <Navbar.Brand href="/home">
            <img className="icon-img logoimg p-2" src={loginicon} alt="icon" />
            <b>Administrator</b>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
              <Link className="lgout" to="/">
                Log Out
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      
        <Container className="mt-5 col-lg-9 userscontainer">
            <h4 className="">News</h4>
        <Table hover>
          <thead>
            <tr>
              <th>Full Name</th>
              <th>Email Address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
              <tr>
                <td>Nyim</td>
                <td>12200067.gcit@rub.edu.bt</td>
                <td> 
                  <Button className="mx-2">Delete</Button>
                </td>
              </tr>

              <tr>
                <td>Nyim</td>
                <td>12200067.gcit@rub.edu.bt</td>
                <td> 
                  <Button className="mx-2">Delete</Button>
                </td>
              </tr>

          </tbody>
        </Table>
      </Container>
        </>

    )
}
export default Users;