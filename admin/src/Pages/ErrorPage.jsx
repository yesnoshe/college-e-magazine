import React from "react";

import Container from "react-bootstrap/Container";
import "../css/error.css";


function ErrorPage() {
  return (
    <Container className="errorpage text-center">
      <h5>Error! Page not Found :( </h5>
    </Container>
  )
  
  
}

export default ErrorPage;
