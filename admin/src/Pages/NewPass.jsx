import React from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import "../css/NewPass.css";
import FloatingLabel from "react-bootstrap/esm/FloatingLabel";

function NewPass() {
  return (
    <Container className="cont">
      <h5 className="mb-4">New Password</h5>
      <Form>
        <FloatingLabel
          label="Enter new Password"
          className="mb-4"
          controlId="formBasicPassword"
        >
          <Form.Control
            className="inp"
            type="password"
            placeholder="Password"
          />
        </FloatingLabel>

        <FloatingLabel
          label="Confirm password"
          className="mb-4"
          controlId="formBasicPassword"
        >
          <Form.Control
            className="inp"
            type="password"
            placeholder="Password"
          />
        </FloatingLabel>
        
        <Button className="btn btnlg w-100 mt-4" type="submit">
          Reset password
        </Button>
      </Form>
    </Container>
  );
}

export default NewPass;
