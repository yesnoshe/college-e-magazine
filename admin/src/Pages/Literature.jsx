import React from "react";

import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import { Link } from "react-router-dom";
import loginicon from "../images/Logo black.jpeg";

import Container from "react-bootstrap/Container";
import Table from "react-bootstrap/Table";
import Button from "react-bootstrap/Button";

import '../css/news.css';

const Literature = () =>{
    return(
        <>
        <Navbar collapseOnSelect expand="lg" className="navadmin mb-5 py-3">
        <Container>
          <Navbar.Brand href="/home">
            <img className="icon-img logoimg p-2" src={loginicon} alt="icon" />
            <b>Administrator</b>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
              <Link className="lgout" to="/">
                Log Out
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      
        <Container className="mt-5 col-lg-9 newscontainer">
            <h4 className="">Literature Work</h4>
        <Table hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Email</th>
              <th>Category</th>
              <th>Title</th>
              <th>File</th>
              {/* <th>Date</th> */}
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            
              <tr>
                <td>Ugyen Tashi</td>
                <td>12190103.gcit@rub.edu.bt</td>
                <td>Literature Work</td>
                <td>A day in GCIT</td>
                <td>dayingcit.pdf</td>
                {/* <td>06/06/2023</td> */}
                <td> 
                  <Button className="mx-2">Accept</Button>
                  <Button className="mx-2">Reject</Button>
                </td>
              </tr>

              <tr>
                <td>Ugyen Tashi</td>
                <td>12190103.gcit@rub.edu.bt</td>
                <td>Literature Work</td>
                <td>A day in GCIT</td>
                <td>dayingcit.pdf</td>
                {/* <td>06/06/2023</td> */}
                <td> 
                  <Button className="mx-2">Accept</Button>
                  <Button className="mx-2">Reject</Button>
                </td>
              </tr>

              

              

              

          </tbody>
        </Table>
      </Container>
        </>

    )
}
export default Literature;