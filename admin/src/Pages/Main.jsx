import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import "../css/Main.css";
import { Link } from "react-router-dom";
import loginicon from "../images/Logo black.jpeg";

function Main() {
  return (
    <div>
      <Navbar collapseOnSelect expand="lg" className="navadmin mb-5 py-3">
        <Container>
          <Navbar.Brand href="/home">
            <img className="icon-img logoimg p-2" src={loginicon} alt="icon" />
            <b>Administrator</b>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="ms-auto">
              <Link className="lgout" to="/">
                Log Out
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Container className="col-lg-10">
        <Link to="/news" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className="pl-5">
             News
            </div>
          </Container>
        </Link>
        <Link to="/conversation" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className=" pl-5">
              <div className="col">Conversation</div>
            </div>
          </Container>
        </Link>
        <Link to="/photography" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className=" pl-5">
              <div className="col">Photography & Art</div>
            </div>
          </Container>
        </Link>
        <Link to="/literature" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className=" pl-5">
              <div className="col">Literature Work</div>
            </div>
          </Container>
        </Link>
        <Link to="/users" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className=" pl-5">
              <div className="col">Manage Users</div>
            </div>
          </Container>
        </Link>
        {/* <Link to="/add" className="col-sm-1 viewbtn">
          <Container className="empcont">
            <div className=" pl-5">
              <div className="col">Create New User</div>
            </div>
          </Container>
        </Link> */}
      </Container>
    </div>
  );
}

export default Main;
