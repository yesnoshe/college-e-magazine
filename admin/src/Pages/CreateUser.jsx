// import React, { useState } from "react";
// import Container from "react-bootstrap/Container";
// import Navbar from "react-bootstrap/Navbar";
// import Nav from "react-bootstrap/Nav";
// import "../css/Main.css";
// import { Link } from "react-router-dom";
// import { db } from "../firebase-config";
// import { collection, addDoc } from "firebase/firestore";
// import Button from "react-bootstrap/Button";
// import Form from "react-bootstrap/Form";
// import FloatingLabel from "react-bootstrap/esm/FloatingLabel";
// import "../css/CreateUser.css";


// function CreateUser() {
//   const [newName, setNewName] = useState("");
//   const [newEmail, setNewEmail] = useState("");
//   const [newNumber, setNewNumber] = useState();
//   const [newRole, setNewRole] = useState("");
//   const [newPassword, setNewPassword] = useState("");

//   const usersCollectionRef = collection(db, "users");

//   const createUser = async () => {
//     await addDoc(usersCollectionRef, {
//       name: newName,
//       email: newEmail,
//       number: newNumber,
//       role: newRole,
//       password: newPassword,
//     });
//   };


//   return (
//     <div>
//       <Navbar collapseOnSelect expand="lg" className="navadmin mb-5  py-3">
//         <Container>
//           <Navbar.Brand href="/home">
//             <b>Administrator</b>
//           </Navbar.Brand>
//           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
//           <Navbar.Collapse id="responsive-navbar-nav">
//             <Nav className="ms-auto">
//               <Link className="lgout" to="/">
//                 Log Out
//               </Link>
//             </Nav>
//           </Navbar.Collapse>
//         </Container>
//       </Navbar>
//       <Container className="mt-3 text-center">
//         <h2>Create New User</h2>
//       </Container>
      
//       <Container className="mt-3 col-lg-4">
//         <Form>
//           <FloatingLabel
//             label="Name"
//             className="mb-3 mt-4"
//             // controlId="formBasicEmail"
//           >
//             <Form.Control
//               onChange={(event) => {
//                 setNewName(event.target.value);
//               }}
//               className="inp"
//               placeholder="Enter email"
//             />
//           </FloatingLabel>
//           <FloatingLabel
//             label="Email"
//             className="mb-3 mt-4"
//             controlId="formBasicEmail"
//           >
//             <Form.Control
//               onChange={(event) => {
//                 setNewEmail(event.target.value);
//               }}
//               className="inp"
//               type="email"
//               placeholder="Enter email"
//             />
//           </FloatingLabel>

//           <FloatingLabel
//             label="Phone Number"
//             className="mb-3 mt-4"
//             controlId="formBasicEmail"
//           >
//             <Form.Control
//               onChange={(event) => {
//                 setNewNumber(event.target.value);
//               }}
//               className="inp"
//               type="number"
//               placeholder="Enter email"
//             />
//           </FloatingLabel>
//           <Form.Select className="selectcustom mb-3 mt-4"
//             onChange={(event) => {
//               setNewRole(event.target.value);
//             }}
//             aria-label="Default select example"
//           >
//             <option>Role</option>
//             <option value="Employer">Employer</option>
//             <option value="Employee">Employee</option>
//           </Form.Select>
//           <FloatingLabel
//             label="Password"
//             className="mb-5"
//             controlId="formBasicPassword"
//           >
//             <Form.Control
//               onChange={(event) => {
//                 setNewPassword(event.target.value);
//               }}
//               className="inp mb-3 mt-4"
//               type="password"
//               placeholder="Password"
//             />
//           </FloatingLabel>

//           <Link className="createbtn mt-3" to="/home">
//             <Button
//               onClick={createUser}
//               className="btnlg"
//               variant="mt-5 w-100"
//               type="submit"
//             >
//               Create User
//             </Button>
//           </Link>
//         </Form>
//       </Container>
//     </div>
//   );
// }

// export default CreateUser;
