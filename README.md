College E-Magazine

College students and faculty members are interested in showcasing their work, experiences and achievements but a traditional print-based magazine can be limiting in terms of accessibility and resources. There is a growing need for an online platform where students and faculty members can share and interact with ideas, stories, events and experiences with a wider audience.

Therefore, College E-Magazine will serve as a creative digital platform which provides a place for students and faculty members to express themselves and engage with their audience in a more dynamic way. The e-magazine will regularly update contents upon submissions from students and faculty members to ensure relevancy and quality of the article contents.

----------------------------------------------------------

**Aim**

To create a digital platform which allows students and faculty members to showcase their creative writing skills with a wider audience in an interactive way.

**Goal**

To develop a web application which allows students and faculty members of the college to share their work, experiences and achievements. 

**Objectives**

1. To allow students/faculty members to share their work and interact with their audience
2. To digitalize the traditional-based college magazine by making it more interactive,dynamic and convenient


