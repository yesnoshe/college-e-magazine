import React from "react";

import {Container,Card, Col, Row} from 'react-bootstrap';
import logo from "../Images/Logo black.jpeg";

const SectionThree = () => {
    return(
        <Container>
            <Row className="mt-4">
                    <p className="headings mt-4">Literature</p>
                    <Container className="">
                    <Row className="mt-3">
                        <Col xs={4}>
                        <Card style={{ width: '20rem', boxShadow: '0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col xs={4}>
                        <Card style={{ width:'20rem',boxShadow: '0 0 10px rgba(0,0,0,0.08)' }}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col xs={4}>
                        <Card style={{ width:'20rem',boxShadow: '0 0 10px rgba(0,0,0,0.08)' }}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col> 
                    </Row>
                    </Container>
            </Row>
        </Container>

    )
}
export default SectionThree;