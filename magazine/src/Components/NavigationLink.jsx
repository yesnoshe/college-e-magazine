import React from "react";
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route
} from "react-router-dom";

import News from "../Pages/News";
import Conversations from "../Pages/Conversations";
import PhotographyAndArt from "../Pages/PhotopgrahyAndArt";
import Literature from "../Pages/Literature";
import SignUp from "../Pages/SignUp";
import SignIn from "../Pages/SignIn";
import Index from "../Pages/Index";
import SubmitYourWork from "../Pages/SubmitYourWork";
import logo from "../Images/Logo black.jpeg";

import "../CSS/NavigationStyles.css"


const NavigationLink = () => {
    return(
        <div>
    <Navbar expand="lg" className="navbar">
      <Container fluid>
        <Navbar.Brand href="/">
        <img
              src={logo}
              width="70"
              height="70"
              className="d-inline-block align-top rounded-circle"
              alt="Magazine logo"
            />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0 mx-5"
            style={{ maxHeight: '100px' }} 
          > 
            <Nav.Link as={Link} to={"/"} className="nav-link">Home</Nav.Link>
            <Nav.Link as={Link} to={"/News"} className="nav-link">News</Nav.Link>
            <Nav.Link as={Link} to={"/Conversations"}>Conversations</Nav.Link>
            <Nav.Link as={Link} to={"/PhotographyAndArt"}>Photography & Art</Nav.Link>
            <Nav.Link as={Link} to={"/Literature"}>Literature</Nav.Link>
            <Nav.Link as={Link} to={"/SubmitYourWork"}>Submit Your Work</Nav.Link>
          </Nav>
          <Form className="d-flex mx-2" style={{width: "400px"}}>
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
          <Nav className="mx-4">
            <Nav.Link as={Link} to={"/SignUp"}>Sign Up</Nav.Link>
            <Nav.Link as={Link} to={"/SignIn"}>Sign In</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </div>
  );
};
export default NavigationLink;