import React from "react";
import {Container,Card, Col, Row} from 'react-bootstrap';
import Styles from '../CSS/SectionOne.css';
import logo from "../Images/Logo black.jpeg";


const SectionOne = () => {
    return(
        <Container>
            <Row className="mt-4">
                <Col xs={12} md={8} className="">
                    <p className="headings mt-4">News</p>
                    <Container className="">
                    <Row className="mt-3">
                        <Col xs={6}>
                        <Card style={{ width:'20rem', boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>News Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col xs={6}>
                        <Card style={{ width:'20rem',boxShadow:'0 0 10px rgba(0,0,0,0.08)' }}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>News Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col xs={6}>
                        <Card style={{ width:'20rem' }}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>News Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                        <Col xs={6}>
                        <Card style={{ width:'20rem' }}>
                            <Card.Img variant="top" src={logo}/>
                            <Card.Body>
                                <Card.Title>News Title</Card.Title>
                            </Card.Body>
                        </Card>
                        </Col>
                    </Row>
                    </Container>
                    
                </Col>
                <Col xs={6} md={4} className="">
                    <p className="headings mt-4">Conversations</p>
                    <Card className="mt-2" style={{ width:'25rem',boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Body>
                                <Card.Title>Conversation one</Card.Title>
                            </Card.Body>
                    </Card>
                    <Card className="mt-2" style={{ width:'25rem', boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Body>
                                <Card.Title>Conversation two</Card.Title>
                            </Card.Body>
                    </Card>
                    <Card className="mt-2" style={{ width:'25rem', boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Body>
                                <Card.Title>Conversation three</Card.Title>
                            </Card.Body>
                    </Card>
                    <Card className="mt-2" style={{ width:'25rem',boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Body>
                                <Card.Title>Conversation four</Card.Title>
                            </Card.Body>
                    </Card>
                    <Card className="mt-2" style={{ width:'25rem',boxShadow:'0 0 10px rgba(0,0,0,0.08)'}}>
                            <Card.Body>
                                <Card.Title>Conversation five</Card.Title>
                            </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
export default SectionOne;