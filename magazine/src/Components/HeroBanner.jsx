import React from "react";
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import {Container, Row, Col} from 'react-bootstrap';
import Styles from '../CSS/HeroBanner.css';

import herobanner from "../Images/herobanner.jpg";

const HeroBanner = () => {
    return(
        <section className="banner" id="home">
            <Container>
                <Row className="align-items-center">
                    <Col xs={12} md={6} xl={7}>
                        <h1>GCIT E-Magazine</h1>
                        <p>
                            An online plateform for students and staff members of the college comunity
                            to share their creative skills with wider audience. 
                        </p>
                        <button>Explore More</button>
                    </Col>
                </Row>
            </Container>
        </section> 
    )
}

export default HeroBanner;
