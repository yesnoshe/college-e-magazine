import React from "react";
import logo from '../Images/logo rounded.png';
import '../CSS/Footer.css';


const Footer = () => {
    return(
        <footer className="footer_container text-white pt-5 pb-4 mt-5">
        <div className="container col-md-12 text-center text-md-left">
            <div className="row text-center text-md-left">
            <div className="col-md-4 col-lg-4 col-xl-4 mx-auto mt-3">
            <img width={80} height={80} className="d-inline-block rounded-circle" alt="logo" src={logo}/>
            <div className="footer_link row text-center mt-2">
                <p>Gyalpozhing College,Monggar</p>
            </div>
            </div>
            <div className="col-md-4 col-lg-4 col-xl-4 mx-auto mt-3">
                <div className="row text-left">
                    <h5 className="footer_link">Quick Links</h5>
                    <a href="https://www.rub.edu.bt/" className="links">Royal University of Bhutan</a>
                    <a href="https://www.gcit.edu.bt/" className="links">Gyalpozhing College of Information Technology</a>
                    <a href="https://www.sherubtse.edu.bt/" className="links">Sherubtse College</a> 
                </div>
            </div>
            <div className="col-md-4 col-lg-4 col-xl-4 mx-auto mt-3">
                <div className="row text-left">
                    <h5 className="footer_link">Downloads</h5>
                    <a href="" className="links mt-2">Class Trip Form</a>
                    <a href="" className="links">Monday Assembly Agenda</a>
                </div>
            </div>
            </div>
            <div className=" footer_link row text-center text-md-left mt-5">
            <p>2023, GCIT College E-Magazine. All rights reserved.</p> 
            </div>
        </div>
    </footer>
    )
}
export default Footer; 