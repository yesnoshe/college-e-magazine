// import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import NavigationLink from './Components/NavigationLink';

import Index from './Pages/Index';
import About from './Pages/About';
import Literature from './Pages/Literature';
import News from './Pages/News';
import Conversations from './Pages/Conversations';
import PhotographyAndArt from './Pages/PhotopgrahyAndArt';
import SubmitYourWork from './Pages/SubmitYourWork';
import SignIn from './Pages/SignIn';
import SignUp from './Pages/SignUp';
import ForgotPassword from './Pages/ForgotPassword';
import OtpPage from './Pages/OtpPage';
import ErrorPage from './Pages/ErrorPage';

import {
  BrowserRouter as Router,
  Routes,
  Link,
  Route,
  useNavigate
} from "react-router-dom";
import AccountActivation from './Pages/AccountActivation';


function App() {
  return (
    <Router>
      <Routes>
        <Route path={"/"} element={<Index/>}/>
        <Route path={"/About"} element={<About/>}/>
        <Route path={"/News"} element={<News />} />
        <Route path={"/Conversations"} element={<Conversations />} />
        <Route path={"/PhotographyAndArt"} element={<PhotographyAndArt />} />
        <Route path={"/Literature"} element={<Literature />} />
        <Route path={"/SubmitYourWork"} element={<SubmitYourWork />} />
        <Route path={"/SignUp"} element={<SignUp />} />
        <Route path={"/AccountActivation"} element={<AccountActivation/>} />
        <Route path={"/SignIn"} element={<SignIn />} />
        <Route path={"/ForgotPassword"} element={<ForgotPassword/>} />
        <Route path={"/OtpPage"} element={<OtpPage/>} />
        <Route path={"/Index"} element={<Index />} />
        <Route path="*" element={<ErrorPage />} />
    </Routes>
    </Router>
  );
}

export default App;
