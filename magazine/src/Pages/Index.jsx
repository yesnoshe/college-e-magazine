import React from "react";
import { ReactDOM } from "react";
import Styles from '../CSS/HeroBanner.css';

import NavigationLink from "../Components/NavigationLink";
import HeroBanner from "../Components/HeroBanner";
import SectionOne from "../Components/SectionOne";
import SectionThree from "../Components/SectionThree";
import Footer from "../Components/Footer";


import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route,
    useNavigate
} from "react-router-dom";

const Index = () => {
    return (
        <>
        <NavigationLink></NavigationLink>
        <HeroBanner></HeroBanner>
        <SectionOne></SectionOne>
        <SectionThree></SectionThree>
        <Footer></Footer> 
        </>
    )
}
export default Index;