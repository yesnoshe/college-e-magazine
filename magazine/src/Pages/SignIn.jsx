import React, {useState} from "react";
import logo from "../Images/logo rounded.png";
import '../CSS/SignInAndSignUp.css';
import Validation from "../Components/Validation";
import SigninValidation from "../Components/SigninValidation";

import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route,
    useNavigate
  } from "react-router-dom";

const SignIn = () => {

    const [values, setValues] = useState({
        email: '',
        password: ''
        
    })
    const [errors,setErrors] = useState({})
    function handleInput(event){
        const newObj = {...values, [event.target.name]: event.target.value}
        setValues(newObj)
    }
    function handleValidation(event){
        event.preventDefault();
        setErrors(SigninValidation(values));
    }
    return(
        <div className="login template d-flex justify-content-center align-items-center 100-w 100-vh mt-5">
            <div className="form_container 40-w p-5 mt-5 rounded bg-light"> 
            <form onSubmit={handleValidation}>
                <h3 className="text-center">Sign in</h3>
                <div className="text-center mt-2 mb-3">
                <img className="img-fluid" width={80} height={80} src={logo}/>
                </div>
                <div className="mb-3">
                    <input type="email" placeholder="Email address" className="form-control "/>
                    {errors.email && <p style={{color: 'red', marginTop:'10px'}}>{errors.email}</p>}

                </div>
                <div className="mb-2">
                    <input type="password" placeholder="Password" className="form-control "/>
                    {errors.password && <p style={{color: 'red', marginTop:'10px'}}>{errors.password}</p>} 
                </div> 
                <p className=" text-left mb-4" style={{fontSize:'14px'}}>
                    <Link to='/ForgotPassword' className="forgotpassword">Forgot password?</Link>
                </p>
                <div className="d-grid">
                    <button className="btn" style={{backgroundColor:'orange'}}>Sign in</button>
                </div>
                <p className="text-end mt-2" style={{fontSize:'14px'}}>
                    Don't have an account?<Link className="signupLink" to='/SignUp'>Sign Up</Link>
                </p>
                <div>
                </div>
            </form>
            </div>
        </div>
    )
}
export default SignIn;