import React from "react";
import logo from "../Images/logo rounded.png";
import '../CSS/SignInAndSignUp.css';


import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route,
    useNavigate
  } from "react-router-dom";

const ForgotPassword = () => {
    return(
        <div className="login template d-flex justify-content-center align-items-center 100-w 100-vh mt-5">
            <div className="form_container 40-w p-5 mt-5 rounded bg-light"> 
            <form>
                <h3 className="text-center">Forgot Password</h3>
                <p>Enter your email to reset your password. An OTP will be sent to your email.</p>
                <div className="mb-5">
                    <input type="email" placeholder="Email address" className="form-control "/>
                </div> 
                <div className="d-grid">
                    <button className="btn" style={{backgroundColor:'orange'}}>Continue</button>
                </div>
                <div>
                </div>
            </form>
            </div>
        </div>
    )
}
export default ForgotPassword;