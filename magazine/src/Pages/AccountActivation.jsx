import React from "react";

const AccountActivation = () => {
    return(
        <>
        <div className="login template d-flex justify-content-center align-items-center 100-w 100-vh mt-5">
            <div className="form_container 40-w p-5 mt-5 rounded bg-light"> 
            <form>
                <h3 className="text-center">Account Activation</h3>
                <p>Enter the OTP that you received on your email to activate your account</p>
                <div className="mb-5">
                    <input type="email" placeholder="Enter OTP code here" className="form-control "/>
                </div> 
                <div className="d-grid"> 
                    <button className="btn" style={{backgroundColor:'orange'}}>Activate</button>
                </div>
                <div>
                </div>
            </form>
            </div>
        </div>
        </>
    )
}
export default AccountActivation;