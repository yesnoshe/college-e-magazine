import React, { useEffect, useState } from "react";

import {Container} from 'react-bootstrap';
import logo from "../Images/logo rounded.png";
import '../CSS/SignInAndSignUp.css';
import Validation from "../Components/Validation";

import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route,
    useNavigate
  } from "react-router-dom";

const SignUp = () => {
    const [values, setValues] = useState({
        fullname: '',
        email: '',
        password: '',
        confirm_password: ''
    })

    const navigate = useNavigate();

    const [errors,setErrors] = useState({})
    function handleInput(event){
        const newObj = {...values, [event.target.name]: event.target.value}
        setValues(newObj)
    }
    function handleValidation(event) {
        event.preventDefault();
        const validationErrors = Validation(values);
        setErrors(validationErrors);
        if (Object.keys(validationErrors).length === 0) {
          // Navigate to the next page if there are no validation errors
          navigate('/AccountActivation');
        }
      }
      
    return(
        <div className="d-flex justify-content-center align-items-center 100-w 100-vh mt-5 ">
            <div className="outer_container 40-w p-5 mt-5 rounded bg-light"> 
            <form onSubmit={handleValidation}> 
                <h3 className="text-center">Create an account</h3>
                <div className="text-center mt-2 mb-3">
                <img className="img-fluid" width={80} height={80} src={logo}/>
                </div>
                <div className="mb-3">
                    <input type="text" placeholder="Full name" name="fullname" className="form-control " onChange={handleInput}/>
                    {errors.fullname && <p style={{color: 'red', marginTop:'10px'}}>{errors.fullname}</p>}
                </div>
                <div className="mb-3">
                    <input type="email" name="email" placeholder="Email address" className="form-control" onChange={handleInput}/>
                    {errors.email && <p style={{color: 'red', marginTop:'10px'}}>{errors.email}</p>}

                </div>
                <div className="mb-3">
                    <input type="password" name="password" placeholder="Password" className="form-control" onChange={handleInput}/>
                    {errors.password && <p style={{color: 'red', marginTop:'10px'}}>{errors.password}</p>}

                </div>
                <div className="mb-3">
                    <input type="password" name="confirm_password" placeholder="Confirm password" className="form-control "/>
                    {/* {errors.confirm_password && <p style={{color: 'red', marginTop:'10px'}}>{errors.confirm_password}</p>} */}

                </div>
                <div className="mb-5">
                    <input type="checkbox" className="custom-control custom-checkbox" id="check"/>
                    <label htmlFor="check" className="custom-input-label"> I agree to be bound by the project service terms</label>
                </div>
                <div className="d-grid">
                    <button className="btn" style={{backgroundColor:'orange'}}>Sign Up</button>
                </div>
                <p className="text-end mt-2" style={{fontSize:'14px'}}> 
                    Already have an account?<Link className="signinLink" to='/SignIn'>Sign In</Link>
                </p> 
                <div>
                </div>
            </form>
            </div>
        </div>
        
    )
}
export default SignUp;