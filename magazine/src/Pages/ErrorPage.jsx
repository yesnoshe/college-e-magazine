import React from "react";

function ErrorPage() {
  return <div className="mt-5 text-center">Error! Page Not Found!</div>;
}

export default ErrorPage;