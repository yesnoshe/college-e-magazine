import React from "react";
import logo from "../Images/logo rounded.png";
import NavigationLink from "../Components/NavigationLink";
import '../CSS/ContentSubmission.css';

import {
    BrowserRouter as Router,
    Routes,
    Link,
    Route,
    useNavigate
  } from "react-router-dom";

const SubmitYourWork = () => {
    return(
        <>
            <NavigationLink/>
            <div className="login template d-flex justify-content-center align-items-center 100-w 100-vh mt-5">
            <div className="form_container 40-w p-5 mt-5 rounded bg-light"> 
            <form>
                <h3 className="text-center">Submit Your Work</h3>
                <div className="mb-3">
                    <input type="text" placeholder="Title of the Content" className="form-control " required/>
                </div>
                <div className="mb-4">
                    <select className="category_container p-2 rounded" required>
                        <option>Select a Category</option>
                        <option>News</option>
                        <option>Literature</option>
                        <option>Conversation</option>
                        <option>Photography & Art</option>
                    </select>
                </div>
                <div className="mt-2 mb-3">
                    <input type="file" className="form-control-file" id="file" required/>
                </div>
                <div className="row">
                    <div className="text-center">
                    <button className="btn mx-3" style={{backgroundColor:'orange'}}>Submit</button>
                    <button className="btn mx-3" style={{backgroundColor:'orange'}}>Cancel</button>
                    </div>
                </div>
                <div>
                </div>
            </form>
            </div>
        </div>
        </>
    )
}
export default SubmitYourWork;